﻿using System;
using System.Linq;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Console.Write("\nEnter a value:\t");
            int a = Convert.ToInt32(Console.ReadLine());

            Console.Write("\nEnter b value:\t");
            int b = Convert.ToInt32(Console.ReadLine());

            Console.Write("\nEnter b value:\t");
            int c = Convert.ToInt32(Console.ReadLine());

            Module1 module1 = new Module1();
            Console.WriteLine(module1.SwapItems(a, b));

            int[] input = { a, b, c };
            Console.WriteLine(module1.GetMinimumValue(input));
        }


        public int[] SwapItems(int a, int b)
        {
            int c = b;
            b = a;
            a = c;
            int[] input = {a, b};
            return input;
        }

        public int GetMinimumValue(int[] input)
        {
            return input.Min();
        }
    }
}
